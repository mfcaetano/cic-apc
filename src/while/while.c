#include <stdio.h>

#define JOGAR 1
#define CONFIGURAR 2
#define SAIR 0

int main(){
	int resposta;

	do{
		printf("=========================\n");
		printf("Menu\n");
		printf("=========================\n");
		printf("1 - Jogar\n");
		printf("2 - Configurar\n");
		printf("0 - Sair\n");
		printf("=========================\n");

		scanf("%d", &resposta);

		switch(resposta){
			case JOGAR:
				printf("Vai lá campeão\n");
				break;
			case CONFIGURAR:
				printf("Configuração\n");
				break;
			case SAIR:
				printf("Só sai!\n");
				break;
			default:
				printf("Entrada Inválida\n");
				break;
		}

	} while( resposta != SAIR);
	
	return 0;
}
