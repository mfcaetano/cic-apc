
#include <stdio.h>

int main(){

	int quantidade;
	int codigo;
	float preco;

	do {
		printf("Informe o código do produto: \n");
		scanf("%d", &codigo);
		printf("Informe a quantidade adquirida:\n");
		scanf("%d", &quantidade);

		switch(codigo){
			case 1:
				preco = 2.98;
				break;
			case 2:
				preco = 4.50;
				break;
			case 3:
				preco = 9.98;
				break;
			case 4:
				preco = 4.49;
				break;
			case 5:
				preco = 6.87;
				break;
			default:
				printf("Código informado é inválido!\n");
		}

		if(quantidade < 0)
			printf("Quantidade deve ser maior ou igual a zero!\n");

	} while( codigo > 5 || codigo < 1 || quantidade < 0 );

	printf("Valor bruto da compra é: %f \n", preco*quantidade);



	return 0;
}
