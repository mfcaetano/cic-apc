#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
	char nome[100];
	float pontos;
} Jogador;

typedef struct{
	Jogador jogadores[100];
	int qt_jogador;
} Ranking;


void imprime_ranking(Ranking* rank);
void salva_ranking(Ranking* rank, char* nome);
void recupera_ranking(Ranking* rank, char *nome);

int main(){
	Ranking jog;
	Ranking jog2;
	
	strcpy(jog.jogadores[0].nome, "Joao");
	jog.jogadores[0].pontos = 5;

	strcpy(jog.jogadores[1].nome, "Paulo");
	jog.jogadores[1].pontos = 10;
	
	strcpy(jog.jogadores[2].nome, "Pedro");
	jog.jogadores[2].pontos = 9;

	jog.qt_jogador = 3;

	imprime_ranking(&jog);

	salva_ranking(&jog, "jogo.bin");

	recupera_ranking(&jog2, "jogo.bin");

	printf("\n Valores Recuperados de Jog2\n");
	imprime_ranking(&jog2);

	return 0;
}


void imprime_ranking(Ranking* rank){
	int i;

	for(i = 0; i < rank->qt_jogador; i++)
		printf("%d %s %f \n", i, rank->jogadores[i].nome, rank->jogadores[i].pontos); 

}

void salva_ranking(Ranking* rank, char* nome){
	FILE* fp = fopen(nome, "wb+");

	if(fp == NULL){
		printf("Lascou!\n");
		exit(1);
	}

	fwrite(rank, sizeof(Ranking), 1, fp);

	fclose(fp);
}


void recupera_ranking(Ranking* rank, char *nome){
	FILE* fp = fopen(nome, "rb");

	if(fp == NULL){
		printf("Lascou de vez!\n");
		exit(1);
	}

	fread(rank, sizeof(Ranking), 1, fp);

	fclose(fp);
}
