#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
	char* nome;
	float pontos;
} Jogador;

typedef struct{
	Jogador *jogadores;
	int qt_jogador;
} Ranking;


void aloca_ranking(Ranking* rank);
void libera_ranking(Ranking* rank);

void imprime_ranking(Ranking* rank);
void salva_ranking(Ranking* rank, char* nome);
void recupera_ranking(Ranking* rank, char *nome);

int main(){
	Ranking *jog  = (Ranking *) malloc(sizeof(Ranking));
	Ranking *jog2 = (Ranking *) malloc(sizeof(Ranking));
	
	aloca_ranking(jog);
	
	strcpy(jog->jogadores[0].nome, "Joao");
	jog->jogadores[0].pontos = 5;

	strcpy(jog->jogadores[1].nome, "Paulo");
	jog->jogadores[1].pontos = 10;
	
	strcpy(jog->jogadores[2].nome, "Pedro");
	jog->jogadores[2].pontos = 9;

	jog->qt_jogador = 3;

	imprime_ranking(jog);

	salva_ranking(jog, "jogo.bin");

	recupera_ranking(jog2, "jogo.bin");

	printf("\n Valores Recuperados de Jog2\n");
	imprime_ranking(jog2);

	libera_ranking(jog);
	libera_ranking(jog2);

	free(jog);
	free(jog2);

	return 0;
}


void aloca_ranking(Ranking* rank){
	int i;
	rank->jogadores = (Jogador *) malloc(100*sizeof(Jogador));

	for(i=0; i < 100; i++)
		rank->jogadores[i].nome = (char *) malloc(50*sizeof(char));
}


void libera_ranking(Ranking* rank){
	int i;

	for(i = 0; i < 100; i++)
		free(rank->jogadores[i].nome);

	free(rank->jogadores);
}

void imprime_ranking(Ranking* rank){
	int i;

	for(i = 0; i < rank->qt_jogador; i++)
		printf("%d %s %f \n", i, rank->jogadores[i].nome, rank->jogadores[i].pontos); 

}

void salva_ranking(Ranking* rank, char* nome){
	FILE* fp = fopen(nome, "wb+");

	if(fp == NULL){
		printf("Lascou!\n");
		exit(1);
	}

	int i;
	fwrite(rank, sizeof(Ranking), 1, fp);
	fwrite(rank->jogadores, 100*sizeof(Jogador), 1, fp);

	for(i=0; i < 100; i++)
		fwrite(rank->jogadores[i].nome, 50*sizeof(char), 1, fp);

	fclose(fp);
}


void recupera_ranking(Ranking* rank, char *nome){
	FILE* fp = fopen(nome, "rb");

	if(fp == NULL){
		printf("Lascou de vez!\n");
		exit(1);
	}

	fread(rank, sizeof(Ranking), 1, fp);

	rank->jogadores = (Jogador *) malloc(100*sizeof(Jogador));

	fread(rank->jogadores, 100*sizeof(Jogador), 1, fp);

	for(i=0; i < 100; i++){
		rank->jogadores[i].nome = (char *) malloc(50*sizeof(char));
		fread(rank->jogadores[i].nome, 50*sizeof(char), 1, fp);
	}

	fclose(fp);
}
