#include <stdio.h>

#define MAX 10
#define LINHA 2
#define COLUNA 2

int main(){
   int numeros[MAX];
   int matriz[LINHA][COLUNA];
   int i, j;
   int* pt = numeros;

   for(i=0; i < MAX; i++){
	numeros[i] = i;
   }

   for(i=0; i < LINHA; i++)
	for(j=0; j < COLUNA; j++)
		matriz[i][j] = 10*j;

   /*for(i=MAX-1; i >= 0; i--){
	printf("numeros[%d]=%d\n", i, numeros[i]);
   } */

   
   for(i=0; i < MAX; i++){
	//printf("numeros[%d]=%d\n", i, *(pt++));
	printf("numeros[%d]=%d\n", i, pt[i]);
   } 

   printf("Valores da Matriz\n");

   for(i=0; i < LINHA; i++)
	for(j=0; j < COLUNA; j++)
		printf("matriz[%d][%d]=%d\n",i,j, matriz[i][j]);
  
   printf("Acessando matriz pelo ponteiro\n");
   pt = matriz[0];

   for(i=0; i < (LINHA * COLUNA); i++)
	printf("matriz[%d]=%d\n", i, *(pt++));

	 
   
}
