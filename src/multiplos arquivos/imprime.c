#include "imprime.h"
#include <stdio.h>

void imprime_numero(char* texto, int num){
	printf("%s %d.\n", texto, num);
}

void imprime_texto(char* texto, char* nome){
	printf("%s %s.\n", texto, nome);
}
