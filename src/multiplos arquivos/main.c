#include <stdio.h>
#include "imprime.h"
#include "calcula.h"

int main(){
	int n1, n2;
	char nome[] = "Marcos Caetano";

	scanf("%d %d", &n1, &n2);

	imprime_numero("Soma: ", soma(n1, n2));
	imprime_numero("Subtração: ", subtrai(n1, n2));
	imprime_numero("Multiplicação: ", multiplica(n1, n2));
	imprime_texto("Nome: ", nome);


}
