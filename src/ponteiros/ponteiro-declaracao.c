#include <stdio.h>

int  main(){
	int a = 4;
	int *pt_int;
	char *pt;

	printf("Valor de A: %d \n", a);

	pt = &a;
	pt_int = &a;

	printf("Valor do Ponteiro (pt): %p \n", pt);
	printf("Valor do Ponteiro (pt_int): %p \n", pt_int);
	printf("Valor referenciado do ponteiro (pt): %c \n", *pt);
	printf("Valor referenciado do ponteiro (pt_int): %d \n", *pt_int);

	*pt = 'a';

	printf("Valor referenciado do ponteiro (pt): %c \n", *pt);
	printf("Valor referenciado do ponteiro (pt_int): %d \n", *pt_int);
	printf("Valor de A: %d \n", a);
}
