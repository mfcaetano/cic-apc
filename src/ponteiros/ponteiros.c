#include <stdio.h>

int main(){


	int num = 4;
	int *pt;

	printf("Valor num=%d \n", num);
	printf("End de num=%p \n", &num);

	pt = &num;

	*pt = 10;

	printf("Valor de Num=%d\n", num);

	printf("Valor pt=%p \n", pt);
	printf("End do pt=%p \n", &pt);


	printf("tamanho int: %ld\n", sizeof(int));
	printf("tamanho ponteiro para int: %ld\n", sizeof(int *));
	printf("tamanho char: %ld\n", sizeof(char));
	printf("tamanho ponteiro para char: %ld\n", sizeof(char *));

	

	return 0;
}
