#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 100
typedef struct{
	int id;
	float peso;
} Registro;

void imprimir_registros(Registro* pesos, int tamanho);
void inicializar_valores(Registro* pesos, int tamanho);

int main(){
	Registro pesos[MAX];
	srand(time(NULL));

	inicializar_valores(pesos, 7);
	imprimir_registros(pesos, 7);

}


void inicializar_valores(Registro* pesos, int tamanho){
	int i;

	for(i=0; i < tamanho; i++){
		pesos[i].id   = i;
		pesos[i].peso = (rand() % 150) + 1;
	}
}

void imprimir_registros(Registro* pesos, int tamanho){
	int i;
	for(i=0; i < tamanho; i++)
		printf("i=%d -> id=%d peso:%f \n", i, pesos[i].id, pesos[i].peso);
	
}
