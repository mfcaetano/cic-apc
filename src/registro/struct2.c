#include <stdio.h>
#include <stdlib.h>


typedef struct{
	char *nome;
	int idade;
} Pessoa;

Pessoa* getPessoa(int tamanho);
void freePessoa( Pessoa* pessoa );
void inicializaPessoa( Pessoa* pessoa );


Pessoa* getPessoa(int tamanho){
	Pessoa* result = (Pessoa *) malloc(sizeof(Pessoa));
	result->nome   = (char *) malloc(tamanho * sizeof(char));

	return result;
}

void freePessoa( Pessoa* pessoa ){
	free(pessoa->nome);
	free(pessoa);
}



int main(){
	Pessoa* contato = getPessoa(20);

	inicializaPessoa(contato);

	printf("Nome Informado: %s \n", contato->nome);
	printf("Idade Informada: %d \n", contato->idade);

	freePessoa(contato);
	
	return 0;
}


void inicializaPessoa( Pessoa* pessoa ){
	printf("Informe nome:\n");
	scanf("%[^\n]", pessoa->nome);

	printf("Informe Idade:\n");
	scanf("%d", &pessoa->idade);
}
