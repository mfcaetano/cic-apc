#include <stdio.h>
#include <stdlib.h>


typedef struct{
	char *nome;
	int idade;
} Pessoa;

int main(){
	Pessoa* contato;
	
	contato      = (Pessoa *) malloc(sizeof(Pessoa));

	(*contato).nome = (char *) malloc(20 * sizeof(char)); 

	printf("Informe nome:\n");
	scanf("%[^\n]", contato->nome);

	printf("Informe Idade:\n");
	scanf("%d", &contato->idade);

	printf("Nome Informado: %s \n", contato->nome);
	printf("Idade Informada: %d \n", contato->idade);

	free(contato->nome); 
	free(contato);
	
	return 0;
}


