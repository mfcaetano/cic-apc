#include <stdio.h>

#define TAMANHO 100

int main(){
	char nome[TAMANHO];
	int i;

	printf("Informe um nome:\n");
	scanf("%[^\n]", nome);

	printf("Nome Informado: %s\n", nome);

	for(i=0; i < TAMANHO; i++){
		if( nome[i] >= 97  && nome[i] <= 122)
			nome[i] = nome[i] - 32;		
	}


	printf("Nome resultante: %s\n", nome);

}
