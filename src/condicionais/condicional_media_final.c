/*
*/
#include <stdio.h>

int main(){

	float media;
	int freq;

	printf("Informe sua média: \n");
	scanf("%f", &media);

	printf("Informe sua frenquencia: \n");
	scanf("%d", &freq);

	if( freq >= 75){
		printf("Você tem frenquencia minima\n");

		printf("Nota final: ");

		if( (media >= 9) && (media <= 10) ){
			printf("SS");
		}else if( (media >= 7) && (media < 9) ){
			printf("MS");	
		}else if( (media >= 5) && (media < 7) ){
			printf("MM");
		}else if( (media >= 3) && (media < 5) ){
			printf("MI");
		}else if( (media > 0) && (media < 3) ){
			printf("II");
		}else if( (media == 0) ){
			printf("SR");
		}else{
			printf("Valor informado inválido: %f \n", media);
		}

		printf("\n");
	}else{
		printf("Você reprovou por falta!\n");
	}

}
