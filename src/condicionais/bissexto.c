#include <stdio.h>
#include <assert.h>

#define DIA_MAX 31
#define DIA_MIN 1
#define FALSO 0
#define NOVO_TIPO long long int

#define SOMA(x) (x+2)

#define NOME "Joao"

int main(){


	int dia, mes, ano;


	printf("%d \n", (2+2));

	printf("Informe o dia: \n");
	scanf("%d", &dia);


	if((dia < DIA_MIN) || (dia > DIA_MAX)){
		printf("O dia: %d informado é inválido! \n", dia);

	 	assert(FALSO);		
	}

	printf("Informe o mês: \n");
	scanf("%d", &mes);

	if((mes < 1) || (mes > 12)){
		printf("O mês:%d informado é inválido! \n", mes);

		assert(FALSO);
	}


	printf("Informe o ano: \n");
	scanf("%d", &ano);

	if(ano < FALSO){
		printf("O ano:%d informado é inválido!\n", ano);

		assert(0);
	}




	return 0;
}
