#include <stdio.h>

void imprimeMsg( int* num, char letra );

int leiaNumero();
void leiaDoisNumeros( int* num1, int* num2);

int main(){
	int numero = 2;
	int num1, num2;

	imprimeMsg(&numero, 'a');

	printf("Valor de Numero na Main(): %d\n", numero);


	numero = leiaNumero();

	leiaDoisNumeros( &num1, &num2);

	printf("Valor num1: %d num2: %d \n", num1, num2);

	printf("Valor lido: %d\n", numero);
}


void leiaDoisNumeros( int* num1, int* num2){
	scanf("%d", num1 );
	scanf("%d", num2 );
}


int leiaNumero(){
	int num;
	scanf("%d", &num);

	return num;
}


void imprimeMsg( int* numero, char letra ){
	printf("Olá Mundo %c\n", letra);
	*numero = 5;
	printf("O numero: %d\n", *numero);
}
