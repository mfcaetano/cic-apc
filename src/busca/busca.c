#include <stdio.h>

#define NAO_ENCONTRADO -1

int busca_sequencial(int* pt, int tamanho, int numero);


int main(){
	int numeros[] = {7, 89, 100, 1, 4, 55, 66, 10, 9, 8};

	if(busca_sequencial(numeros, 10, 128) != NAO_ENCONTRADO )
		printf("Número encontrado!\n");
	
	return 0;
}



int busca_sequencial(int* pt, int tamanho, int numero){
	if( pt == NULL || tamanho <= 0)
		return -1;
	int i;

	for(i=0; i < tamanho-1; i++)
		if( pt[i] == numero )
			return i;


	return -1;
}
